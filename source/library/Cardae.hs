module Cardae (run) where

import FRP.Yampa.Extra

import Cardae.Objects
import qualified Cardae.Camera.State as Camera
import qualified Cardae.Mesh.State as Mesh
import qualified Cardae.Behaviour as Game


makeGameInput :: YampaSDL.AppInput ~> ObjectInput
makeGameInput = proc appInput -> do
    returnA -< ObjectInput appInput


game' :: GameState -> Game
game' gs = proc objectInput -> do
    newObjectStates <- parB initialObjects -< objectInput
    returnA -< gs
        { gameStateEntities = map objectOutputState newObjectStates
        }
  where
    initialObjectStates :: [ObjectState]
    initialObjectStates = gs^.entities

    initialObjects :: [Object]
    initialObjects = fmap Game.behave initialObjectStates

initialCamera :: Camera.State
initialCamera = Camera.State
    { Camera.stateEye = V3 (-ratio) ratio (-1.0)
    , Camera.statePosition = V3 1 1 (-1)
    , Camera.stateRotation = 0
    }
  where
    ratio = 800 / 600

initialTriangle :: Mesh.State
initialTriangle = Mesh.State
    { Mesh.stateCenter = V3 0 0 0
    , Mesh.stateVertices =
            [ ("#FF0000", V3 (-0.6) (-0.4) 0)
            , ("#00FF00", V3 0.6 (-0.4) 0)
            , ("#0000FF", V3 0 0.6 0)
            ]
    }

initGraphics :: YampaSDL.SDLConfiguration -> IO (YampaSDL.SDLInit (Event SDL.EventPayload) GameState)
initGraphics config = do
    SDL.initialize [SDL.InitVideo]
    SDL.HintRenderScaleQuality $= SDL.ScaleLinear
    renderQuality <- SDL.get SDL.HintRenderScaleQuality
    when (renderQuality /= SDL.ScaleLinear) (putTextLn "Linear filtering not enabled!")

    window <- SDL.createWindow (toText $ YampaSDL.windowName config) windowConfiguration

    SDL.showWindow window

    void $ SDL.glCreateContext window

    lastInteraction <- newMVar =<< SDL.time
    return $ YampaSDL.SDLInit
        { initAction = do
                return NoEvent

        , inputAction =
                YampaSDLInput.inputAction lastInteraction

        , parseInput =
            YampaSDLParse.parseInput

        , outputAction =
            outputAction' window

        , closeAction = do
                SDL.destroyWindow window
                SDL.quit
        }
  where
    windowWidth = fromIntegral $ YampaSDL.windowWidth config
    windowHeight = fromIntegral $ YampaSDL.windowHeight config
    windowConfiguration = SDL.defaultWindow
        { SDL.windowInitialSize = V2 ( fromIntegral windowWidth ) ( fromIntegral windowHeight )
        , SDL.windowOpenGL = Just SDL.defaultOpenGL
        }

    outputAction' :: SDL.Window -> Bool -> GameState -> IO Bool
    outputAction' window _ gs = do
        GL.viewport $= (GL.Position 0 0, GL.Size windowWidth windowHeight)
        GL.clear [GL.ColorBuffer]
        mapM_ render (gs^.entities)
        SDL.glSwapWindow window
        return (gs^.shouldQuit)

run :: IO ()
run = do
    initSDL <- initGraphics YampaSDL.defaultSDLConfiguration
              { YampaSDL.windowWidth = 800
              , YampaSDL.windowHeight = 600
              , YampaSDL.windowName = "Cardae"
              }
    let gs = GameState
             { gameStateEntities = [CameraState initialCamera, MeshState initialTriangle]
             , gameStateShouldQuit = False
             }
    reactimate
        -- Initial action
        (YampaSDL.initAction initSDL)
        -- Input sensing action
        (YampaSDL.inputAction initSDL)
        -- Output processing action
        (YampaSDL.outputAction initSDL)
        -- Signal function
        ( YampaSDL.parseInput initSDL
          >>> makeGameInput
          >>> game' gs
        )
