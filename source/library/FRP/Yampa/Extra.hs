module FRP.Yampa.Extra where

import FRP.Yampa

type a ~> b = SF a b
