module Cardae.Camera.Render where

import Cardae.Camera.State

render :: State -> IO ()
render cs = do
    GL.matrixMode $= GL.Projection
    GL.loadIdentity
    GL.ortho
        (cs^.eye^._x)
        (cs^.eye^._y)
        (cs^.eye^._z)
        (cs^.position^._x)
        (cs^.position^._y)
        (cs^.position^._z)

    GL.matrixMode $= GL.Modelview 0
    GL.loadIdentity
    GL.rotate
        (cs^.rotation)
        (GL.Vector3 0 0 1 :: GL.Vector3 GL.GLdouble)
