module Cardae.Camera.Behaviour where

import Cardae.Objects
import Cardae.Camera.State

behave :: State -> Object
behave cs = proc objectInput -> do
    let input' = objectInput^.gameInput
    key <- YampaSDL.anyKeyActive -< input'
    returnA -< ObjectOutput . CameraState $
        event cs (applyEventToCamera cs) key

applyEventToCamera :: State -> [SDL.Scancode] -> State
applyEventToCamera cs scanCodes
    | any dKeyIsPressed scanCodes = cs&rotation%~ (+ 1)
    | any aKeyIsPressed scanCodes = cs&rotation%~ (+ -1)
    | otherwise                   = cs

dKeyIsPressed :: SDL.Scancode -> Bool
dKeyIsPressed SDL.ScancodeD = True
dKeyIsPressed _ = False

aKeyIsPressed :: SDL.Scancode -> Bool
aKeyIsPressed SDL.ScancodeA = True
aKeyIsPressed _ = False
