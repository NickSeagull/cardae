module Cardae.Camera.State where

import Cardae.Common

data State = State
    { stateEye      :: Position
    , statePosition :: Position
    , stateRotation :: Double
    }
makeFields ''State
