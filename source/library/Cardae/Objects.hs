module Cardae.Objects where

import FRP.Yampa.Extra

import qualified Cardae.Camera.State as Camera
import qualified Cardae.Mesh.State as Mesh
import qualified Cardae.Camera.Render as Camera
import qualified Cardae.Mesh.Render as Mesh


data ObjectState
    = CameraState Camera.State
    | MeshState Mesh.State

render :: ObjectState -> IO ()
render (CameraState s) = Camera.render s
render (MeshState s) = Mesh.render s

data GameInput = GameInput
    { gameInputInput :: YampaSDL.AppInput
    }
makeFields ''GameInput

data GameState = GameState
    { gameStateEntities :: [ObjectState]
    , gameStateShouldQuit :: Bool
    }
makeFields ''GameState


data ObjectInput = ObjectInput
    { objectInputGameInput :: YampaSDL.AppInput
    }
makeFields ''ObjectInput


data ObjectOutput = ObjectOutput
    { objectOutputState :: ObjectState
    }
makeFields ''ObjectOutput


type Game   = ObjectInput ~> GameState
type Object = ObjectInput ~> ObjectOutput
