module Cardae.Mesh.State where


import Cardae.Common

data State = State
    { stateCenter   :: Position
    , stateVertices :: [(String, Position)]
    }
makeFields ''State

absoluteVertices :: State -> [(String, Position)]
absoluteVertices state =
    let
        addCenter vertex = (fst vertex, state^.center ^+^ snd vertex)
    in
        map addCenter (state^.vertices)
