module Cardae.Mesh.Behaviour where

import Cardae.Objects
import Cardae.Mesh.State

behave :: State -> Object
behave cs = proc _ ->
    returnA -< ObjectOutput $ MeshState cs
