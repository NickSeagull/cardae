module Cardae.Mesh.Render where

import Cardae.Mesh.State

render :: State -> IO ()
render s = GL.renderPrimitive GL.Triangles $ do
    forM_ (absoluteVertices s) $ \vertex -> do
        let Colour.RGB r g b =  readColor (fst vertex)
        GL.color (GL.Color3 r g b :: GL.Color3 GL.GLdouble)
        GL.vertex $ GL.Vertex3
            (vertex^._2^._x)
            (vertex^._2^._y)
            (vertex^._2^._z)
    GL.flush
  where
    readColor :: String -> Colour.RGB Double
    readColor = Colour.toSRGB . Colour.sRGB24read
