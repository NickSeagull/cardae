module Cardae.Behaviour where

import Cardae.Objects
import qualified Cardae.Camera.Behaviour as Camera
import qualified Cardae.Mesh.Behaviour as Mesh

behave :: ObjectState -> Object
behave (CameraState s) = Camera.behave s
behave (MeshState s) = Mesh.behave s
